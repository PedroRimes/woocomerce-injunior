<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LFfAtS+gQoUJHoxPW5jGkCaVBF51rMC+jXWE/PxJtGm+aIA+jUTyzZKdOEf+jMaTDN/s3nueVnMTK3aGkcu9RA==');
define('SECURE_AUTH_KEY',  'R44e76y5EzQ66FS8MBBaYfKzkJcMTNJ5PRckH4a7/YHnVGWMusWhprh3DaxcoOVPm53L5gR+5DKtzkrtc9GZkg==');
define('LOGGED_IN_KEY',    'yDWwE9VDD7WZw96zxcwHEqTm7+eMP+AK1n1C7o/3zaHo3Ym29VmGvp+/+/vN0CGA9X7gh4q7K5A4jJYjDwc7qg==');
define('NONCE_KEY',        '7Bv5I0eNVl0c/fa+9ReIYdeiFLjByA7pljxPksj7L3BU5jZUUcheVJM+XpkqlwRZrsXRi4CIk25dLSGqBUQkQA==');
define('AUTH_SALT',        '7fbD6Yf9pm9tVfPAF0QsM6Lcr09jS5TujlEqDuLqY+vkf4KgqusMxY0STF2aY4+lBMe8wuMLYx/+NMAvm1NT9A==');
define('SECURE_AUTH_SALT', 'pFqiyRzdw9JYrdHv3g8rLUyIOFta8jAs28qHCkghuUXJTRw2t2bZm+xDzeH2PjUCSPQaZHUIt3EliFLW8LgDTA==');
define('LOGGED_IN_SALT',   'ZgCgNw01qye+y+9u5My9tQzRDtXV261Y4Vp5trRKFgB7162xCj0XmHr12tMHAcYAX8ke66zzOGwRRYYJUItbjw==');
define('NONCE_SALT',       'cxxdS5jUX2cawChZRolSLrLFW21NgTK9i4AUOUaHYdo7HHC69LKy0IX26Zy2TwhcHThF9rmoKMaE4IwJJl9mCQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
